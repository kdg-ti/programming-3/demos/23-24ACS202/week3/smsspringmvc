package be.kdg.programming3.smsspringmvc.repository;

import be.kdg.programming3.smsspringmvc.domain.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class HardcodedStudentRepository implements StudentRepository {
    private Logger logger = LoggerFactory.getLogger(HardcodedStudentRepository.class);
    private static List<Student> students = new ArrayList<>();

    @Override
    public Student createStudent(Student student){
        if (student==null) {
            logger.error("Student should never be null!");
        }
        logger.info("Adding the student {} to the database...", student);
        student.setId(students.size());
        students.add(student);
        return student;
    }

    @Override
    public List<Student> readStudents() {
        logger.info("Reading the students...");
        return students;
    }
}
