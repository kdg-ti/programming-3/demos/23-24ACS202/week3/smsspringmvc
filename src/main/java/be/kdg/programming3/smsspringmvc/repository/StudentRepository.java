package be.kdg.programming3.smsspringmvc.repository;

import be.kdg.programming3.smsspringmvc.domain.Student;

import java.util.List;

public interface StudentRepository {
    Student createStudent(Student student);

    List<Student> readStudents();
}
