package be.kdg.programming3.smsspringmvc.repository;

import be.kdg.programming3.smsspringmvc.domain.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DataFactory implements CommandLineRunner {
    private StudentRepository studentRepository;

    public DataFactory(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void seed(){
        studentRepository.createStudent(new Student("Jos", LocalDate.of(1978,3,5),1.78));
        studentRepository.createStudent(new Student("Annemie", LocalDate.of(1978,3,5),1.78));
        studentRepository.createStudent(new Student("An", LocalDate.of(1988,3,5),1.38));
        studentRepository.createStudent(new Student("Fien", LocalDate.of(1978,2,5),1.79));
    }

    @Override
    public void run(String... args) throws Exception {
        seed();
    }
}
