package be.kdg.programming3.smsspringmvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsspringmvcApplication {

    public static void main(String[] args) {
        SpringApplication.run(SmsspringmvcApplication.class, args);
    }

}
