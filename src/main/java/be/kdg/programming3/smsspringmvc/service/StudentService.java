package be.kdg.programming3.smsspringmvc.service;

import be.kdg.programming3.smsspringmvc.domain.Student;

import java.time.LocalDate;
import java.util.List;

public interface StudentService {
    Student addStudent(String name, LocalDate birthday, double length);

    List<Student> getAllStudents();
}
