package be.kdg.programming3.smsspringmvc.presentation;

import be.kdg.programming3.smsspringmvc.domain.Student;
import be.kdg.programming3.smsspringmvc.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/students")
public class StudentController {
    private Logger logger = LoggerFactory.getLogger(StudentController.class);
    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public String getStudentsView(Model model) {
        logger.info("Request for students view!");
        List<Student> studentList = studentService.getAllStudents();
        model.addAttribute("students", studentList);
        return "students";//return the logical name of a view
    }

    @GetMapping("/add")
    public String showAddStudentForm(Model model){
        return "addstudent";
    }

    @PostMapping("/add")
    public String processAddStudentForm(String name, double length){
        studentService.addStudent(name, LocalDate.now(), length);
        return "redirect:/students";
    }
}
